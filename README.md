# GenVec

GenVec is a Vector container utilizing [generational indices](https://lucassardois.medium.com/generational-indices-guide-8e3c5f7fd594) as keys. A generational index is essentially a tuple of an index and a generation. If an index is removed and/or replaced, the generation is incremented, making this useful when keys are kept after the value it references has potentially been removed.

This library is written in, and thus supports TypeScript, but is compatible with JavaScript as well.

## API

### `new GenVec<TValue>(values?: GenElement<TValue>[])`

Create a new GenVec, optionally with a set of values. Values in the GenVec internally have the definition:

```ts
interface GenElement<TValue> {
    generation: number;
    value: TValue | null;
}
```

### `grow(size: number)`

Grow the vector to the provided size. If the vector length is already equal to or greater than the provided size it will do nothing. Otherwise the extra space will be filled with empty values.

Example:

```ts
const vec = new GenVec();
vec.grow(5);
console.log(vec.length); // Outputs "5"
```

### `shrink(size: number)`

Shrinks the vector to the specified size. If the vector length is already equal to or smaller than the provided size it will do nothing, otherwise all extra values past the provided size will be dropped.

Example:

```ts
const vec = new GenVec();
vec.insert("foo");
vec.insert("bar");
vec.shrink(1);
console.log(vec.length); // Outputs "1"
```

### `insert(value: TValue): GenKey`

Insert a value and receive a key to retrieve it.

Example:

```ts
const vec = new GenVec();
const key = vec.insert("foo");
console.log(vec.get(key)); // Outputs "foo"
```

### `remove(key: GenKey)`

Remove a value, emptying it's value and increasing the generation.

Example:

```ts
const vec = new GenVec();
const key = vec.insert("foo");
vec.remove(key);
console.log(vec.get(key)); // Outputs "null"
```

### `get(key: GenKey): TValue | null`

Get a value from the vector by key. If the generation in the provided key doesn't match it will be considered invalid and return None.

Example:

```ts
const vec = new GenVec();
const key = vec.insert("foo");
console.log(vec.get(key)); // Outputs "foo"
vec.remove(key); // Value is set to null and generation is incremented from 0 to 1
console.log(vec.get(key)); // Outputs "null"
```

### `set(key: GenKey, value: TValue)`

Set a value in the vector by key. If they generation in the provided key doesn't match it will be considered invalid and the value won't be changed.

Example:

```ts
const vec = new GenVec();
const key = vec.insert("foo");
vec.set(key, "bar");
console.log(vec.get(key)); // Outputs "bar"
```

### `replace(key: GenKey, value: TValue)`

Replaces an element, ignoring any generation mismatch. This is primarily useful for syncing with another GenVec.

Example:

```ts
const vec = new GenVec();
const key = vec.insert("foo");
const otherVec = new GenVec();
otherVec.insert("bar");
otherVec.replace(key, "baz");
console.log(otherVec.get(key)); // Outputs "baz"
```

### `forEach(fn: (value: TValue, key: GenKey) => void)`

Iterate over each non-empty value in the vector.

Example:

```ts
const vec = new GenVec();
vec.insert("foo");
vec.insert("bar");
vec.forEach((value) => console.log(value)); // Outputs "foo", "bar"
```

### `map(fn: (value: TValue, key: GenKey) => TValue | null): GenVec<TValue>`

Perform a map operation over non-empty values in the vector. Returns a new GenVec with updated values.

Example:

```ts
const vec = new GenVec();
vec.insert("foo");
vec.insert("bar");
const otherVec = vec.map((value) => `${value}${value}`);
vec.forEach((value) => console.log(value)); // Outputs "foofoo", "barbar"
```