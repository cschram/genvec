import { GenVec } from ".";

describe("grow", () => {
    it("should grow the vector", () => {
        const vec = new GenVec<string>();
        vec.grow(5);
        expect(vec.length).toEqual(5);
    });
});

describe("shrink", () => {
    it("should shrink the vector", () => {
        const vec = new GenVec<string>();
        vec.grow(5);
        vec.shrink(3);
        expect(vec.length).toEqual(3);
    });
});

describe("insert", () => {
    it("should insert an element and return a key", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        expect(key).toStrictEqual({
            generation: 0,
            index: 0,
        });
    });

    it("should insert an element in an empty element", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        vec.remove(key);
        const newKey = vec.insert("bar");
        expect(key).toStrictEqual({
            generation: 0,
            index: 0,
        });
        expect(newKey).toStrictEqual({
            generation: 1,
            index: 0,
        });
        expect(vec.get(newKey)).toEqual("bar");
    });
});

describe("remove", () => {
    it("should remove an element", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        vec.remove(key);
        expect(vec.get(key)).toBeNull();
    });
});

describe("set", () => {
    it("should update an element value", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        vec.set(key, "bar");
        expect(vec.get(key)).toEqual("bar");
    });

    it("should not update the element with an invalid key", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        vec.set({ generation: 1, index: 0 }, "bar");
        expect(vec.get(key)).toEqual("foo");
    });
});

describe("replace", () => {
    it("should replace an element value", () => {
        const vec = new GenVec<string>();
        const key = vec.insert("foo");
        vec.remove(key);
        vec.insert("bar");
        vec.replace(key, "foo");
        expect(vec.get(key)).toEqual("foo");
    });

    it("should grow the vector when out of range", () => {
        const vec = new GenVec<string>();
        const key = {
            generation: 1,
            index: 4,
        };
        vec.replace(key, "foo");
        expect(vec.length).toEqual(5);
        expect(vec.get(key)).toEqual("foo");
    });
});

describe("forEach", () => {
    it("should iterate over non-empty values", () => {
        const vec = new GenVec<string>();
        const keyOne = vec.insert("foo");
        const keyTwo = vec.insert("bar");
        const keyThree = vec.insert("baz");
        vec.remove(keyTwo);
        const fn = jest.fn();
        vec.forEach(fn);
        expect(fn).toBeCalledTimes(2);
        expect(fn.mock.calls[0]).toEqual(["foo", keyOne]);
        expect(fn.mock.calls[1]).toEqual(["baz", keyThree]);
    });
});

describe("map", () => {
    it("should iterate and map non-empty values", () => {
        const vec = new GenVec<string>();
        const keyOne = vec.insert("foo");
        const keyTwo = vec.insert("bar");
        const keyThree = vec.insert("baz");
        vec.remove(keyTwo);
        const fn = jest.fn(((value) => `${value}${value}`));
        const newVec = vec.map(fn);
        expect(fn).toBeCalledTimes(2);
        expect(fn.mock.calls[0]).toEqual(["foo", keyOne]);
        expect(fn.mock.calls[1]).toEqual(["baz", keyThree]);
        expect(newVec.length).toEqual(3);
        expect(newVec.get(keyOne)).toEqual("foofoo");
        expect(newVec.get(keyTwo)).toBeNull();
        expect(newVec.get(keyThree)).toEqual("bazbaz");
    });
});
