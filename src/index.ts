export interface GenKey {
    index: number;
    generation: number;
}

interface GenElement<TValue> {
    generation: number;
    value: TValue | null;
}

/**
 * Generational Vector container, manages vector elements with generational indices.
 * A useful container when keys might stick around longer than a value is valid for.
 * The generation associated with an index can account for when a key points to an
 * older, invalid value.
 * <https://lucassardois.medium.com/generational-indices-guide-8e3c5f7fd594>
 */
export class GenVec<TValue> {
    constructor(
        private values: GenElement<TValue>[] = [],
    ) {}
    
    /**
     * Get the length of the vector.
     * @returns The length.
     */
    get length(): number {
        return this.values.length;
    }

    /**
     * Grow the vector to the provided size. If the vector length is already equal to
     * or greater than the provided size it will do nothing. Otherwise the extra
     * space will be filled with empty values.
     * @param size Size to expand the vector to.
     */
    grow(size: number) {
        if (size > this.length) {
            const iterations = size - this.length;
            for (let i = 0; i < iterations; i++) {
                this.values.push({
                    generation: 0,
                    value: null,
                });
            }
        }
    }

    /**
     * Shrinks the vector to the specified size. If the vector length is already equal
     * to or smaller than the provided size it will do nothing, otherwise all extra
     * values past the provided size will be dropped.
     * @param size Size to shrink the vector to.
     */
    shrink(size: number) {
        if (size < this.length) {
            this.values = this.values.slice(0, size);
        }
    }

    /**
     * Insert a value and receive a key to retrieve it.
     * @param value Value to insert
     * @returns Generational key to retrieve the value later.
     */
    insert(value: TValue): GenKey {
        const emptyIndex = this.values.findIndex((entry) => entry.value == null);
        if (emptyIndex > -1) {
            // Use an empty 
            this.values[emptyIndex].value = value;
            return {
                generation: this.values[emptyIndex].generation,
                index: emptyIndex,
            };
        } else {
            this.values.push({
                generation: 0,
                value: value,
            });
            return {
                generation: 0,
                index: this.values.length - 1,
            };
        }
    }

    /**
     * Remove a value, emptying it's value and increasing the generation.
     * @param key Generational key whose value is to be removed.
     */
    remove(key: GenKey) {
        if (
            key.index < this.length &&
            this.values[key.index].generation === key.generation
        ) {
            this.values[key.index] = {
                generation: key.generation + 1,
                value: null,
            };
        }
    }

    /**
     * Get a value from the vector by key. If the generation in the provided
     * key doesn't match it will be considered invalid and return None.
     * @param key Generational key to lookup a value for.
     * @returns An option either containing the value or None.
     */
    get(key: GenKey): TValue | null {
        if (
            key.index < this.length &&
            this.values[key.index].generation === key.generation
        ) {
            return this.values[key.index].value;
        } else {
            return null;
        }
    }

    /**
     * Set a value in the vector by key. If they generation in the provided
     * key doesn't match it will be considered invalid and the value won't
     * be changed.
     * @param key Generational key to update the value for.
     * @param value The new value to update.
     */
    set(key: GenKey, value: TValue) {
        if (
            key.index < this.length &&
            this.values[key.index].generation === key.generation
        ) {
            this.values[key.index].value = value;
        }
    }
    
    /**
     * Replaces an element, ignoring any generation mismatch.
     * This is primarily useful for syncing with another GenVec.
     * @param key Generational key for the element to replace.
     * @param value Value to replace with.
     */
    replace(key: GenKey, value: TValue) {
        if (key.index >= this.length) {
            this.grow(key.index + 1);
        }
        this.values[key.index] = {
            generation: key.generation,
            value: value,
        };
    }

    /**
     * Iterate over each non-empty value in the vector.
     * @param fn Function to be called for each value.
     */
    forEach(fn: (value: TValue, key: GenKey) => void) {
        this.values.forEach((elem, index) => {
            if (elem.value) {
                fn(
                    elem.value,
                    {
                        generation: elem.generation,
                        index,
                    }
                );
            }
        });
    }

    /**
     * Perform a map operation over non-empty values in the vector.
     * @param fn Mapping function to be called for each non-empty value.
     * @returns A new GenVec with updated values.
     */
    map(fn: (value: TValue, key: GenKey) => TValue | null): GenVec<TValue> {
        return new GenVec<TValue>(
            this.values.map((elem, index) => {
                if (elem.value) {
                    const key = {
                        generation: elem.generation,
                        index,
                    };
                    return {
                        generation: elem.generation,
                        value: fn(elem.value, key),
                    };
                } else {
                    return elem;
                }
            })
        );
    }
}
